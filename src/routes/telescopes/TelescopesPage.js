
import React, {useState }  from 'react';

import {Container, Row, Col } from 'react-bootstrap'

import WSRTCard from '../../components/cards/WSRTCard'
import LOFARCard from '../../components/cards/LOFARCard'
import SKACard from '../../components/cards/SKACard'

export default function TelescopesPage(props) {

    return (
        <div >
            <Container fluid>

                <Row>
                    <Col sm={3} md={3} lg={3}>
            <WSRTCard />
                    </Col>
                    <Col sm={3} md={3} lg={3}>
            <LOFARCard />
                    </Col>
                    <Col sm={3} md={3} lg={3}>
            <SKACard />
                    </Col>
                </Row>
            </Container>
        </div>
    );

}