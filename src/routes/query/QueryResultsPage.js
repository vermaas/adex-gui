
import React, {useState }  from 'react';
import { Card } from 'react-bootstrap'

import { useGlobalReducer } from '../../Store';
import LoadingSpinner from '../../components/LoadingSpinner';

import QueryOutputResultsGrid from './QueryOutputResultsGrid'
import QueryOutputResultsTiles from './QueryOutputResultsTiles'

export default function QueryResultsPage(props) {
    // use the global state

    const [ my_state , my_dispatch] = useGlobalReducer()

    let renderSpinner
    if (my_state.status_run_query === "fetching") {
        renderSpinner = <LoadingSpinner/>
    }

    // get the query results from the state
    let renderOutputResults

    const renderQueryResults = (query) => {
        if (query.output_format.toUpperCase()==='TILES') {
            return <QueryOutputResultsTiles query={query}/>
        } else {
            return <QueryOutputResultsGrid query={query}/>
        }
    }

    if (my_state.queries_have_results) {
        if (my_state.queries_to_run.length > 0) {

            let datasetsToQuery = my_state.queries_to_run
            let data = []

            // combine the results from different queries.
            // iterate through the results of the several queries

            renderOutputResults = my_state.queries_to_run.map(query => {
                let key = "fetched_query_results." + query.dataset_name
                let resultsPerQuery = my_state[key]

                if (resultsPerQuery !== undefined) {
                    if (resultsPerQuery.length > 0) {
                        //alert(key+" : " +resultsPerQuery)
                        return renderQueryResults(query)
                    }
                }
            })

        }
    }

    return (
        <div >
            {renderSpinner}
            {renderOutputResults}
        </div>
    );
}