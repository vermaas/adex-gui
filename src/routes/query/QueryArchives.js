
import React, {useState }  from 'react';

import { Redirect } from "react-router-dom"
import { Button } from 'react-bootstrap'

// https://react-jsonschema-form.readthedocs.io/en/latest/
import Form from "react-jsonschema-form";

import LayoutField from "react-jsonschema-form-layout-grid"

import { GetBackendHost } from '../../utils/web'
import { useGlobalReducer } from '../../Store';
import { useLocalStorage } from '../../hooks/useLocalStorage';

import {
    SET_ESAP_QUERY,
    SET_FETCHED_QUERY_INPUT,
    SET_FETCHED_QUERY_RESULTS,
    SET_STATUS_RUN_QUERY,
    SET_QUERIES_HAVE_RESULTS
} from '../../reducers/GlobalStateReducer'

import { useFetchCreateQuery } from '../../hooks/useFetchCreateQuery';

import { GetQuerySchema } from '../../utils/config'

export default function QueryArchives(props) {

    // react hooks
    const [redirect, setRedirect] = useState(false)
    const [storedQueryUrl, setStoredQueryUrl] = useLocalStorage('esap_url_archives','')
    const [storedQueryJson, setStoredQueryJson] = useLocalStorage('esap_json_archives','')

    // custom hooks
    const [my_state, my_dispatch] = useGlobalReducer()

    const backend_host = GetBackendHost()
    const fetchCreateQuery = useFetchCreateQuery(getBackendUrl("create-query"), {})

    const log = (type) => console.log.bind(console, type);

    const my_query_schema = GetQuerySchema()
    let my_schema = JSON.parse(JSON.stringify(my_query_schema)); // deepcopy, this doesn't alter schema_archives later

    const fields = {
        layout: LayoutField
    }

    const uiSchema = {
        "runId_start": {"ui:widget": {"color": "red"}},
    }

    function getBackendUrl(resource) {
        return backend_host.concat(resource)
    }

    function myObjectFieldTemplate({TitleField, properties, title, description}) {
        return (
            <div>
                <TitleField title={title}/>
                <div className="row">
                    {properties.map(prop => (
                        <div
                            className="col-lg-2 col-md-4 col-sm-6 col-xs-12"
                            key={prop.content.key}>
                            {prop.content}
                        </div>
                    ))}
                </div>
                {description}
            </div>
        );
    }

    // construct the query.
    // note: this is a bit of business logic where query keywords are used
    const constructQuery = (formData) => {
        let query = ""
        let queryToStore = {}
        let key
        let value

        if (formData.institute) {

            key = "institute"
            value = formData.institute.trim()

            if (value !== 'all') {
                query = query + "&" + key + "=" + value
            }
            queryToStore[key]=value
        }

         if (formData.title) {

            key = "title"
            value = formData.title.trim()

            query = query + "&" + key + "=" + value
            queryToStore[key]=value
        }

         if (formData.keyword) {

            key = "keyword"
            value = formData.keyword.trim()

            query = query + "&" + key + "=" + value
            queryToStore[key]=value
        }


        if (formData.target) {

            key = "target"
            value = formData.target.trim()

            query = query + "&" + key + "=" + value
            queryToStore[key]=value
        }


        if (formData.fov) {
            if (formData.fov > 0) {

                // RA, dec query is only useful when fov > 0
                if (formData.ra) {
                    key = "ra"
                    value = formData.ra

                    query = query + "&" + key + "=" + value
                    queryToStore[key]=value
                }

                if (formData.dec) {
                    key = "dec"
                    value = formData.dec

                    query = query + "&" + key + "=" + value
                    queryToStore[key]=value
                }

                key = "fov"
                value = formData.fov

                query = query + "&" + key + "=" + value
                queryToStore[key]=value

            }
        }

        if (formData.dataproduct_type) {

            key = "dataproduct_type"
            value = formData.dataproduct_type.trim()

            if (value !== 'all') {
                query = query + "&" + key + "=" + value
            }
            queryToStore[key]=value
        }

        if (formData.dataproduct_subtype) {

            key = "dataproduct_subtype"
            value = formData.dataproduct_subtype.trim()

            if (value !== 'all') {
                query = query + "&" + key + "=" + value
            }
            queryToStore[key]=value
        }

        if (formData.instrument) {

            key = "instrument"
            value = formData.instrument.trim()

            if (value !== 'all') {
                query = query + "&" + key + "=" + value
            }
            queryToStore[key]=value
        }

        if (formData.startdate) {

            key = "startdate"
            value = formData.startdate.trim()

            if (value !== 'all') {
                query = query + "&" + key + "=" + value
            }
            queryToStore[key]=value
        }

        if (formData.enddate) {

            key = "enddate"
            value = formData.enddate.trim()

            if (value !== 'all') {
                query = query + "&" + key + "=" + value
            }
            queryToStore[key]=value
        }

        // cut off the leading &
        query = query.substr(1)

        // encode it
        query = encodeURI(query)

        // dispatch the query as state to the global store
        my_dispatch({"type": SET_ESAP_QUERY, esap_query: query})

        // store the query in localStorage for later retrieval
       setStoredQueryJson(queryToStore)

        return query
    }


    // handle the submit and dispatch an action accordingly
    const queryESAPBackend = (formData) => {

        // construct the query to the ESAP backend (REST API)
        let query = constructQuery(formData)

        // execute the create-query hook with the new url
        let new_url = getBackendUrl("create-query") + '?' + query

        // store the query in localStorage for retrieval later
        setStoredQueryUrl(new_url)

        // fetch the constructed queries from the ESAP-gateway
        fetchCreateQuery.fetchData(new_url)

        // indicate that the query operation is finished and redirect to the next screen.
        setRedirect(true)
    }


    // handle the submit and dispatch an action accordingly
    const handleSubmit = ({formData}, e) => {

        // clear previous results
        my_dispatch({type: SET_FETCHED_QUERY_INPUT, fetched_query: undefined})

        //my_dispatch({type: SET_FETCHED_QUERY_RESULTS, fetched_query_results: {}})

        // this clears the output results grid/tiles of the executed queries
        my_dispatch({type: SET_STATUS_RUN_QUERY, status_run_query: undefined})
        my_dispatch({type: SET_QUERIES_HAVE_RESULTS, queries_have_results: false})

        let queries = my_state.queries_to_run
        if (queries) {
            for (var i = 0; i < queries.length; i++) {
                my_dispatch({type: SET_STATUS_RUN_QUERY, status_run_query: queries[i].dataset_name + ":n/a"})

            }
        }
        queryESAPBackend(formData)
    }


    const handleError = ({errors}, e) => {
        alert('errors: '+errors)
    }

    const handleClearQuery = () => {
        // clear the query by clearing the localStorage values

        // reload the values
        injectCurrentQuery(my_schema, '')

        // dispatch the state....
        // this also triggers the render, because changing a state with a hook rerenders
        my_dispatch({type: SET_ESAP_QUERY, esap_query: ''})

        // reset the fetched_query to undefined, this also clears the list in the QueryPage
        my_dispatch({type: SET_FETCHED_QUERY_INPUT, fetched_query: undefined})

        // clear the query results
        my_dispatch({type: SET_FETCHED_QUERY_RESULTS, fetched_query_results: {}})

        // this clears the output results grid/tiles of the executed queries
        my_dispatch({type: SET_STATUS_RUN_QUERY, status_run_query: undefined})

        setStoredQueryUrl('')
        setStoredQueryJson('')
    }

    // the current query parameters that are stored in localStorage are injected into the json that feeds the query
    const injectCurrentQuery = (my_schema, my_values) => {

        if (my_values) {

            // loop through all the values in the 'my_values' structure
            // and save them to the 'default' field of the currently used json schema.

            // first extract all the keys and values from 'my_values' into a 'entries' array
            const saved_entries = Object.entries(my_values)

            // destructure the 'entries' array into a json object for easy access
            let saved_values = {}
            for (const [parameter, value] of saved_entries) {
                saved_values[parameter] = value
            }

            // gather all the keys from the currently used query json schema
            const keys = Object.keys(my_schema.properties)

            // loop through the query json schema and look for similar keys in the saved structure
            for (const key of keys) {
                my_schema.properties[key].default = saved_values[key]
            }

        } else {

            // my_values is empty, empty the fields and reset to the original defaults
            my_schema = JSON.parse(JSON.stringify(my_query_schema));
        }
    }

    // --- RENDER ---

    // the current query parameters that are stored in localStorage are injected into the json that feeds the query
    injectCurrentQuery(my_schema, storedQueryJson)

    // if redirect is set (by the submit button) then redirect to the results page
    let renderRedirect
    if (redirect) {
        // todo: redirect this to a results page
        renderRedirect = <Redirect to="/query"/>
    }

    let renderStoredQueryUrl = storedQueryUrl

    return (
        <div >
            {renderRedirect}

            <Form schema={my_schema}
                  uiSchema={uiSchema}
                  onChange={log("changed")}
                  ObjectFieldTemplate={myObjectFieldTemplate}
                  onSubmit={handleSubmit}
                  onError={handleError} >

                <Button type="submit" variant="outline-primary">Create Queries</Button>&nbsp;
                <Button variant="outline-primary" onClick={() => handleClearQuery()}>Clear</Button>&nbsp;
            </Form>

        </div>
    );


}
