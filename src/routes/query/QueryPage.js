
import React, {useState }  from 'react';
import { Card } from 'react-bootstrap'

import { useGlobalReducer } from '../../Store';
import LoadingSpinner from '../../components/LoadingSpinner';
import QueryArchives from './QueryArchives'
import QueryInputResultsGrid from './QueryInputResultsGrid'
import QueryResultsPage from './QueryResultsPage'

export default function QueryPage(props) {
    // use the global state
    const [ my_state , my_dispatch] = useGlobalReducer()

    let renderQueryForm

    // render the query form, only when the database is fetched
    if (my_state.fetched_archives!==undefined) {
        renderQueryForm = <div>
            <Card className="card-query">
                <Card.Body>
                    <QueryArchives />
                </Card.Body>
            </Card>
        </div>
    }

    let renderSpinner
    if (my_state.status_run_query === "fetching") {
        renderSpinner = <LoadingSpinner/>
    }

    // render the results of the 'create query' button in a list.
    let renderQueryInputResultsGrid
    if (my_state.fetched_query!==undefined) {
        renderQueryInputResultsGrid = <QueryInputResultsGrid data = {my_state.fetched_query} />
    }

    // render the results of the 'run query' button (in a list or tiles, depending on how that is defined in 'dataset'
    let renderQueryResults
    //alert(my_state.queries_are_running)
    if (my_state.queries_have_results) {
        if (my_state.queries_to_run.length > 0) {
            renderQueryResults = <QueryResultsPage/>
        }
    }

    return (
        <div >

            {renderSpinner}
            {renderQueryForm}
            {renderQueryInputResultsGrid}
            {renderQueryResults}
        </div>
    );
}