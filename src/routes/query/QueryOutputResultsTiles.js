import React, { useMemo, useState, useCallback, useEffect } from 'react';
import { Link } from "react-router-dom"
import { Button, Card, Row, Col, Container } from 'react-bootstrap';

import { useGlobalReducer } from '../../Store';
import { SET_ACTIVE_DATASET } from '../../reducers/GlobalStateReducer'
import { getBackendUrl } from '../../utils/web'

import QueryCard from '../../components/cards/QueryCard'
import AddToBasketButton from '../../components/buttons/AddToBasketButton'

import Tiles from '../../components/Tiles'

export default function QueryOutputResultsTiles(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    let data
    let title = "Query Results (Tiles)"

    // combine the results from different queries.
    // iterate through the results of the several queries
    data = []

    if (props.query) {
        // filter the data on dataset
        title = "Query Results: " + props.query.dataset_name
        let key = "fetched_query_results." + props.query.dataset_name
        data = my_state[key]
    } else

    if (props.data) {
        // a data selection is given
        data = props.data

    } else {
        // get the list of datasets to query from the global state
        // (currently this option is no longer used)
        my_state.queries_to_run.forEach(dataset => {
            let key = "fetched_query_results." + dataset.dataset_name
            let resultsPerQuery = my_state[key]

            if (resultsPerQuery !== undefined) {
                if (resultsPerQuery.length > 0) {
                    data = data.concat(resultsPerQuery)
                }
            }
        })
    }

    let renderData
    if (my_state.queries_have_results) {
        renderData = <Tiles data = {data} />
    }

    let queryCard = <QueryCard query={props.query}/>

    return (
        <div>
            {queryCard}
            <div className="App">

                <Container fluid>
                    <Row>
                        <Col sm={12} md={12} lg={12}>
                            <div>
                                {renderData}
                            </div>
                        </Col>
                    </Row>
                </Container>


            </div>
        </div>
    );
}