import React from 'react';
import {Card, Table } from 'react-bootstrap'

export default function QueryInputResultList(props) {

    return (
        <Card>
            <Table>
                <thead>
                <th>dataset</th>
                <th>url</th>
                <th>protocol</th>
                <th>query</th>
                <th>remark</th>
                </thead>
                <tbody>
                {props.items.map((item, index) => (
                    <tr>
                        <td>{item.dataset}</td>
                        <td>{item.service_url}</td>
                        <td>{item.protocol}</td>
                        <td><a href={item.query} target="_blank">{item.query}</a></td>
                        <td>{item.remark}</td>
                    </tr>
                ))}
                </tbody>
            </Table>
        </Card>
    );
}