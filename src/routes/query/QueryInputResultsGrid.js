import React, { useMemo, useState, useCallback, useEffect } from 'react';
import { Link } from "react-router-dom"
import { Button, Card } from 'react-bootstrap';
import DataTable from 'react-data-table-component';

import { useGlobalReducer } from '../../Store';
import { SET_QUERIES_TO_RUN,  } from '../../reducers/GlobalStateReducer'

import RunQueriesButton from '../../components/buttons/RunQueriesButton'


export default function QueryInputResultsGrid(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    // useState hooks
    const [selectedRows, setSelectedRows] = useState([]);
    const [toggleCleared, setToggleCleared] = React.useState(false);


    const handleRowSelected = React.useCallback(state => {
        // https://www.npmjs.com/package/react-data-table-component#18-selectable-rows
        // https://jbetancur.github.io/react-data-table-component/?path=/story/selectable-rows--selected-row-toggle

        setSelectedRows(state.selectedRows);

        let queries_to_run = []
        for (var i=0; i< state.selectedRows.length; i++) {
            queries_to_run.push(state.selectedRows[i])
        }

        // store the list of datasets to query. This can later be used as keys to iterate over the results
        my_dispatch({type: SET_QUERIES_TO_RUN, queries_to_run: queries_to_run})

        // invalidate the current selection


    }, []);


    // if 'result' contains 'http' then make it a hyperlink, otherwise just text
    const getStatus = (row) => {
        let value = row.dataset

        try {
            let key = "status_run_query." + row.dataset_name
            let status  = my_state[key]
            let count = ''

            if (status.includes('fetched')) {
                key = "fetched_query_results." + row.dataset_name
                try {
                    count = " (" + my_state[key].length.toString() + ")"
                } catch (e) {
                    count = " (0)"
                }
            }

            value = status + count

        } catch (e) {
            //alert(e.toString())
            value = 'n/a'
        }

        return <div>
            {value}
        </div>

    }

    const columns = [
        {
            name: 'ID',
            selector: 'query_id',
            sortable: true,
            width: "10%"
        },
        {
            name: 'dataset',
            selector: 'dataset_name',
            sortable: true,
            width: "15%"
        },
/*
        {
            name: 'Waveband',
            selector: 'waveband',
            sortable: true,
            width: "10%"
        },
*/
/*
        {
            name: 'service',
            selector: 'esap_service',
            sortable: true,
            width: "5%"
        },

        {
            name: 'service connector',
            selector: 'service_connector',
            sortable: true,
            width: "10%"
        },
*/
        {
            name: 'status',
            sortable: true,
            width: "10%",
            cell: row => getStatus(row),
        },
        {
            name: 'access_url',
            sortable: true,
            width: "20%",
            selector: 'service_url',
        },
/*
        {
            name: 'resource',
            selector: 'resource_name',
            sortable: true,
            width: "10%"
        },
*/
/*
        {
            name: 'query',
            selector: 'query',
            sortable: true,
            cell: row =>
                <a href={row.query} >
                    {row.query}&nbsp;
                </a>,
        },
*/
        {
            name: 'Reference',
            selector: 'reference_url',
            sortable: true,
            cell: row =>
                <a href={row.reference_url} target="_blank">
                    {row.reference_url}&nbsp;
                </a>,
        },

    ];

    const customStyles = {
        headCells: {
            style: {
                fontSize: '16px',
            },
        },
    };

    // the 'Run Queries' button appears when rows are selected.
    const contextActions = React.useMemo(() => {
        return <RunQueriesButton queries={selectedRows}/>;
    }, [selectedRows, toggleCleared]);

    const conditionalRowStyles = [
        {
            when: row => row.status == 'fetched',
            style: {
                color: 'green',
            },

        },
        {
            when: row => row.quality == 'good',
            style: {
                backgroundColor: '#C0FFC0',
                color: 'black',
                fontWeight: 'bold',
                '&:hover': {
                    cursor: 'pointer',
                },
            },

        },
        {
            when: row => row.quality == 'medium',
            style: {
                backgroundColor: 'lightgrey',
                color: 'black',
                '&:hover': {
                    cursor: 'pointer',
                },
            },
        }
    ];


    const countQueryStatus = (search_status) => {
        let queries = my_state.queries_to_run
        let count = 0

        if (queries) {
            for (var i = 0; i < queries.length; i++) {
                try {
                    let key = "status_run_query." + queries[i].dataset_name
                    let status = my_state[key]

                    if (status.includes(search_status)) {
                        count = count + 1
                    }
                } catch (e) {
                    // alert(e)
                }
            }
        }
        return count
    }


    let number_selected = 0
    if (my_state.queries_to_run) {
        number_selected = my_state.queries_to_run.length
    }

    let subtitle = ""
    if (number_selected > 0) {
        subtitle = " fetched " + countQueryStatus('fetched').toString() + " of " + number_selected.toString()
    }
    let title = "Generated Queries: " + subtitle

    return (
        <div>
            <h4>&nbsp;&nbsp;{title}</h4>
            <DataTable
                //title="Created Queries"
                columns={columns}
                data={props.data}
                striped={true}
                pagination
                customStyles={customStyles}
                selectableRows
                onSelectedRowsChange={handleRowSelected}
                contextActions={contextActions}
                conditionalRowStyles={conditionalRowStyles}
            />
        </div>
    );
}