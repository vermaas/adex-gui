import React, { useMemo, useState, useCallback, useEffect } from 'react';
import { Link } from "react-router-dom"
import { Button, Card } from 'react-bootstrap';
import DataTable from 'react-data-table-component';

import { useGlobalReducer } from '../../Store';
import { SET_ACTIVE_DATASET } from '../../reducers/GlobalStateReducer'
import { getBackendUrl } from '../../utils/web'

import AddToBasketButton from '../../components/buttons/AddToBasketButton'
import DataProductImageCard from '../../components/cards/DataProductImageCard'
import QueryCard from '../../components/cards/QueryCard'

// the definition of the columns
// moved outside of the main function so that it will not rerender


export default function QueryOutputResultsGrid(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    // useState hooks
    const [selectedRows, setSelectedRows] = useState([]);
    const [toggleCleared, setToggleCleared] = React.useState(false);

    const handleRowSelected = React.useCallback(state => {
        // https://www.npmjs.com/package/react-data-table-component#18-selectable-rows
        // https://jbetancur.github.io/react-data-table-component/?path=/story/selectable-rows--selected-row-toggle

        setSelectedRows(state.selectedRows);

    }, []);


    // if 'result' contains 'http' then make it a hyperlink, otherwise just text
    function drawResult(row) {
        let value = row.result

        if (value.includes('http')) {
            if (value.endsWith('.png')) {
                return <DataProductImageCard url={value} />

            } else {
                return <a href={value} target="_blank">
                    {value}&nbsp;
                </a>
            }
        } else {
            return <div>
                {value}
            </div>
        }
    }

    // if 'url' contains 'http' then make it a hyperlink, otherwise just text
    function drawUrl(row) {
        // url is not mandatory, only check if available
        if (row.url !== undefined) {
            let value = row.url

            if (value.includes('http')) {
                if (value.endsWith('.png')) {
                    return <DataProductImageCard url={value}/>

                } else {
                    return <a href={value} target="_blank">
                        {value}&nbsp;
                    </a>
                }
            } else {
                return <div>
                    {value}
                </div>
            }
        }
    }

    const columns = [
        {
            name: 'Title',
            selector: 'title',
            sortable: true,
            width: "20%"
        },
        {
            name: 'Result',
            selector: 'result',
            sortable: true,
            cell: row => drawResult(row),
            //width: "20%"
        },
        {
            name: 'Link to data',
            selector: 'url',
            sortable: true,
            cell: row => drawUrl(row),
            width: "45%"
        },
        {
            cell: row =>
                <AddToBasketButton input_query={row}/>,
            button: true,
            width: "10%",
        },

    ];

    const customStyles = {

        headCells: {
            style: {
                fontSize: '16px',
            },
        },
    };


    const contextActions = React.useMemo(() => {
        return <AddToBasketButton queries={selectedRows}/>;
    }, [selectedRows, toggleCleared]);

    // get the query results from the state
    let datasetsToQuery = my_state.queries_to_run
    let data

    if (props.query) {
        // dataset is given as a parameter (current situation)
        data = my_state["fetched_query_results." + props.query.dataset_name]
    } else

    if (props.data) {
        // data is given as a parameter (currently not used)
       data = props.data

    } else {

        // combine the results from different queries.
        // iterate through the results of the several queries
        data = []

        datasetsToQuery.forEach(dataset => {
            let key = "fetched_query_results." + dataset.dataset_name
            let resultsPerQuery = my_state[key]

            if (resultsPerQuery!==undefined) {
                if (resultsPerQuery.length > 0) {
                    data = data.concat(resultsPerQuery)
                }
            }
        })

        // alert('total length = ' + data.length.toString())
    }

    let queryCard = <QueryCard query={props.query}/>

    return (
        <div>
            {queryCard}
            <DataTable
                columns={columns}
                data={data}
                striped={true}
                pagination
                paginationPerPage="50"
                customStyles={customStyles}

                selectableRows
                onSelectedRowsChange={handleRowSelected}
                contextActions={contextActions}
            />
        </div>
    );
}