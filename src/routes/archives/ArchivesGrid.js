import React from 'react';
import { Link } from "react-router-dom"
import { Button, Card } from 'react-bootstrap';
import DataTable from 'react-data-table-component';

import { useGlobalReducer } from '../../Store';
import { SET_ACTIVE_ARCHIVE } from '../../reducers/GlobalStateReducer'
import { getBackendUrl } from '../../utils/web'

import DetailsButton from '../../components/buttons/ArchiveDetailsButton'
import ApiButton from '../../components/buttons/ArchiveApiButton'

import DescriptionCard from '../../components/cards/DescriptionCard'
import ArchiveRetrievalCard from '../../components/cards/ArchiveRetrievalCard'
import ScienceCard from '../../components/cards/ScienceCard'

export default function ArchivesGrid(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const handleClick = (archive) => {
        // dispatch current observation to the global store
        my_dispatch({type: SET_ACTIVE_ARCHIVE, archive: archive})
    }

    // generate the api link
    const getAPI = (archive) => {
        let url = getBackendUrl("archives/")
        let api_link = url + archive.id
        return api_link
    }

    // generate the details link to forward to
    const getLink = (archive) => {
        let details_link = "/archive/"+archive.uri
        return details_link
    }

    const getArchiveLink = (archive) => {
        let link = "/archive/"+archive.name
        return link
    }

    const getTelescopeLink = (archive) => {
        let link = "/telescope/"+archive.instrument
        return link
    }

    const columns = [
        {
            name: 'Archive',
            selector: 'name',
            sortable: true,
            cell: row =>
                <Link to={() => getLink(row)}>
                    {row.name}&nbsp;
                </Link>,
        },

        {
            name: 'Telescope',
            selector: 'instrument',
            sortable: true,
           // cell: row =>
           //     <Link to={() => getTelescopeLink(row)}>
           //         {row.instrument}&nbsp;
           //     </Link>,
        },
        {
            name: 'Catalog',
            selector: 'catalog_name',
            sortable: true,
            cell: row =>
                <Link to={() => getArchiveLink(row)}>
                    {row.catalog_name}&nbsp;
                </Link>,
        },
        {
            name: 'Description',
            selector: 'short_description',
            sortable: true,
        },

        {
            cell: row =>
                <DetailsButton archive={row}/>,

            button: true,
        },
        {
            cell: row =>
                <ApiButton archive={row}/>,
            button: true,
        },
    ];

    const myTheme = {
        title: {
            fontSize: '22px',
            //fontColor: '#FFFFFF',
            //backgroundColor: '#363640',
        },
        contextMenu: {
            //backgroundColor: '#E91E63',
            //fontColor: '#FFFFFF',
        },
        header: {
            fontSize: '16px',
            //fontColorActive: 'FFFFFF',
            //fontColor: '#FFFFFF',
            //backgroundColor: '#363640',
            //borderColor: 'rgba(255, 255, 255, .12)',
        },
        rows: {
            //fontColor: '#FFFFFF',
            //backgroundColor: '#363640',
            //borderColor: 'rgba(255, 255, 255, .12)',
            //hoverFontColor: 'black',
            //hoverBackgroundColor: 'rgba(0, 0, 0, .24)',
        },
        cells: {
            cellPadding: '48px',
        },
        pagination: {
            //fontSize: '13px',
            //fontColor: '#FFFFFF',
            //backgroundColor: '#363640',
            //buttonFontColor: '#FFFFFF',
            //buttonHoverBackground: 'rgba(255, 255, 255, .12)',
        },
        expander: {
            //fontColor: '#FFFFFF',
            //backgroundColor: '#363640',
            //expanderColor: '#FFFFFF',
        },
        footer: {
            //separatorColor: 'rgba(255, 255, 255, .12)',
        },
    };

    const conditionalRowStyles = [
        {
            when: row => row.processing_level == 'processed',
            style: {
                backgroundColor: '#ebffe6',
                color: 'black',
                fontWeight: 'bold',
                '&:hover': {
                    cursor: 'pointer',
                },
            },

        },
        {
            when: row => row.processing_level == 'rraw',
            style: {
                backgroundColor: '#d4d4d4',
                color: 'black',
                fontWeight: 'bold',
                '&:hover': {
                    cursor: 'pointer',
                },
            },

        },

    ];


    // this creates an 'expand' icon in front of every row and shows additional information (images)
    const ExpandableComponent = ({ data }) => <div>
        <Card>
            <DescriptionCard archive={data}/>
            <ScienceCard archive={data}/>
            <ArchiveRetrievalCard archive={data}/>
        </Card>
        &nbsp;
        <img src={data.thumbnail} height={200} />;


    </div>;

    return (
        <div>
            <DataTable
                //title="Astron DataSources"
                columns={columns}
                data={props.data}
                conditionalRowStyles={conditionalRowStyles}
                pagination
                customTheme={myTheme}

                expandableRows
                expandableRowsComponent={<ExpandableComponent />}
            />
        </div>
    );
}