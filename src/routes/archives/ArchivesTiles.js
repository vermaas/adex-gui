import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

import ArchiveCard from '../../components/cards/ArchiveCard'

// loop through a list of observations and create a Card with a clickable thumbnail for all of them
export default function ArchivesTiles(props) {

    return (
        <div>
            <Container fluid>
                <Row>
                    {
                        props.data.map((archive) => {
                            let key = "archive-"+archive.id
                            return (
                                <Col key = {key} sm={1} md={3} lg={6}>
                                    <ArchiveCard archive = {archive} />
                                </Col>
                            );
                        })
                    }
                </Row>
            </Container>
        </div>
    );

}

