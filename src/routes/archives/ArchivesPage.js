
import React, {useState }  from 'react';

import {Card } from 'react-bootstrap'

import { useGlobalReducer } from '../../Store';
import LoadingSpinner from '../../components/LoadingSpinner';
import ArchivesTiles from './ArchivesTiles'
import ArchivesGrid from './ArchivesGrid'

export default function ArchivesPage(props) {
    // use the global state
    const [ my_state , my_dispatch] = useGlobalReducer()

    // conditionally render the archives depending on the statys
    let renderArchives
    let renderArchivesGrid
    if (my_state.status==='fetched_archives') {
        renderArchives = <div>
            <Card>
                <Card.Body>
                    <ArchivesTiles data={my_state.fetched_archives} />
                </Card.Body>
            </Card>
        </div>

        renderArchivesGrid= <div>
            <ArchivesGrid data={my_state.fetched_archives}/>
        </div>
    }

    let renderSpinner
    if (my_state.status === "fetching") {
        renderSpinner = <LoadingSpinner/>
    }

    return (
        <div >
            {renderSpinner}
            {renderArchives}
        </div>
    );


}