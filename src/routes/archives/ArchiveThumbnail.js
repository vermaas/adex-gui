import React from 'react';
import {Card, Button } from 'react-bootstrap'
import { Link } from "react-router-dom"
data
import { SET_ACTIVE_ARCHIVE } from '../../reducers/GlobalStateReducer'
import DescriptionCard from '../../components/cards/DescriptionCard'
import RetrievalCard from '../../components/cards/ArchiveRetrievalCard'

// display a single archive on a card
export default function ArchiveThumbnail(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const handleClick = (archive) => {
        // dispatch current archive to the global store
        my_dispatch({type: SET_ACTIVE_ARCHIVE, archive: archive})
    }

    // generate the details link to forward to
    const getLink = (archive) => {
        let details_link = "/archive/"+archive.uri
        return details_link
    }

    return (

        <Card >


            <Card >
                <h2>{props.archive.name}</h2>
                    <img src={props.archive.thumbnail} height={200} width={200} />
                    <DescriptionCard archive = {props.archive} />
                    <ArchiveRetrievalCard archive = {props.archive} />
                    <Link to={() => getLink(props.archive)}>
                        <Button variant="info" onClick={() => handleClick(props.archive)}>Details</Button>&nbsp;
                    </Link>

            </Card>

        </Card>

    );

}

