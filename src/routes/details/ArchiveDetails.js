import React from 'react';
import { Container, Row, Col, Card, Table } from 'react-bootstrap';

import { useGlobalReducer } from '../../Store';

import ImageCard from '../../components/cards/ImageCard'
import DescriptionCard from '../../components/cards/DescriptionCard'
import ArchiveRetrievalCard from '../../components/cards/ArchiveRetrievalCard'
import DataSetsCard from '../../components/cards/DataSetsCard'

import { url } from '../../components/Main'

export default function ArchiveDetails(props) {

    // get the archive info from the global state.
    const [ my_state , my_dispatch] = useGlobalReducer()

    // find the current archive in the fetched list by uri
    function findElement(arr, propName, propValue) {
        for (var i=0; i < arr.length; i++) {
            if (arr[i][propName] === propValue) {
                return arr[i];
            }
        }
    }

    if (my_state.status !== 'fetched_archives') {
        return null
    }

    let archive = findElement(my_state.fetched_archives,"uri",props.uri)


    return (

        <div>
            <tr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<td><h2>Archive - {archive.name}</h2> </td></tr>
            <Container fluid>

                <Row>
                    <Col sm={3} md={3} lg={3}>
                        <Card>
                            <Table striped bordered hover size="sm">
                                <tbody>
                                <tr>
                                    <td className="key">Instrument</td>
                                    <td className="value">{archive.instrument}</td>
                                </tr>
                                <tr>
                                    <td className="key">Description</td>
                                    <td className="value">{archive.short_description}</td>
                                </tr>
                                </tbody>
                            </Table>
                        </Card>
                        <Card>
                            <ImageCard key={archive.id} archive = {archive} />

                            <DescriptionCard archive = {archive} />
                        </Card>

                    </Col>
                    <Col sm={9} md={9} lg={9}>
                        <Card>
                            <ArchiveRetrievalCard archive = {archive} />
                        </Card>
                        <DataSetsCard archive = {archive} />
                    </Col>

                </Row>

            </Container>

        </div>
    );
}