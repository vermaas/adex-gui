import React from 'react';
import { Link } from "react-router-dom"
import { Button, Card } from 'react-bootstrap';
import DataTable from 'react-data-table-component';

import { useGlobalReducer } from '../../Store';

import DataSetDetailsButton from '../../components/buttons/DataSetDetailsButton'
import DataSetApiButton from '../../components/buttons/DataSetApiButton'

import DescriptionCard from '../../components/cards/DescriptionCard'
import DataSetRetrievalCard from '../../components/cards/DataSetRetrievalCard'


export default function DataSetsGrid(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    // if 'url' contains 'http' then make it a hyperlink, otherwise just text
    function drawUrl(row) {
        // url is not mandatory, only check if available
        let value = row.catalog_user_url_derived

        if (value) {

            if (value.includes('http')) {
                return <a href={value} target="_blank">
                    {value}&nbsp;
                </a>

            } else {
                return <div>
                    {value}
                </div>
            }
        }
    }


    const columns_per_archive = [
        {
            name: 'DataSet',
            selector: 'name',
            sortable: true,
            width: "20%"
        },
        {
            name: 'Catalog Name',
            selector: 'catalog_name_derived',
            sortable: true,
            width: "20%"
        },
        {
            name: 'Catalog Access',
            selector: 'catalog_user_url_derived',
            sortable: true,
            cell: row => drawUrl(row),
        },
    ];

    let columns = [
        {
            name: 'DataSet',
            selector: 'name',
            sortable: true,
            width: "20%"
        },
        {
            name: 'Institute',
            selector: 'institute',
            sortable: true,
            width: "10%"
        },
        {
            name: 'Catalog Name',
            selector: 'catalog_name_derived',
            sortable: true,
            width: "20%"
        },
        {
            name: 'Catalog Access',
            selector: 'catalog_user_url_derived',
            sortable: true,
            cell: row => drawUrl(row),
        },

        {
            cell: row =>
                <DataSetApiButton dataset={row}/>,
            button: true,
        },

    ];

    const customStyles = {

        headCells: {
            style: {
                fontSize: '16px',
            },
        },
    };

    const conditionalRowStyles = [
        {
            when: row => row.processing_level == 'processed',
            style: {
                backgroundColor: '#ebffe6',
                color: 'black',
                fontWeight: 'bold',
                '&:hover': {
                    cursor: 'pointer',
                },
            },

        },
        {
            when: row => row.processing_level == 'rraw',
            style: {
                backgroundColor: '#d4d4d4',
                color: 'black',
                fontWeight: 'bold',
                '&:hover': {
                    cursor: 'pointer',
                },
            },

        },

    ];

    const isExpanded = row => row.defaultExpanded;

    const expandRow = (row) => {
        row.defaultExpanded = true;
    }

    // this creates an 'expand' icon in front of every row and shows additional information (images)
    const ExpandableComponent = ({ data }) => <div>
        <Card>
            <DescriptionCard dataset={data}/>
            <DataSetRetrievalCard archive={data}/>
        </Card>
    </div>;

    if (!props.all_columns) {
        columns = columns_per_archive
    }

    if (props.expand) {

        props.data.map(row => expandRow(row))
    }

    return (
        <div>
            <DataTable
                title="DataSets"
                columns={columns}
                data={props.data}
                conditionalRowStyles={conditionalRowStyles}
                pagination
                paginationPerPage="50"
                striped={true}
                customStyles={customStyles}
                expandableRows
                expandableRowExpanded={isExpanded}
                expandableRowsComponent={<ExpandableComponent />}

            />
        </div>
    );
}