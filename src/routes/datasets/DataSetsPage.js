
import React, {useState }  from 'react';

import { useGlobalReducer } from '../../Store';
import { getBackendUrl, GetBackendHost } from '../../utils/web'
import LoadingSpinner from '../../components/LoadingSpinner';
import DataSetsGrid from './DataSetsGrid'
import { useFetchDataSets } from '../../hooks/useFetchDataSets';

export default function DataSetsPage(props) {
    // use the global state
    const [ my_state , my_dispatch] = useGlobalReducer()

    useFetchDataSets(getBackendUrl("datasets-uri"),{onMount:true})

    // conditionally render the archives depending on the statys

    let renderDataSetsGrid
    if (my_state.status_datasets==='fetched_datasets') {
        renderDataSetsGrid= <div>
            <DataSetsGrid data={my_state.fetched_datasets} expand={false} all_columns={true} />
        </div>
    }

    let renderSpinner
    if (my_state.status_datasets === "fetching_datasets") {
        renderSpinner = <LoadingSpinner/>
    }

    return (
        <div >
            {renderSpinner}
            {renderDataSetsGrid}
        </div>
    );


}