import React, {useState, useEffect }  from 'react';
import '../App.css';

import { GetBaseName } from '../utils/config'

import { NavigationBar } from './NavigationBar';
import ArchivesPage from '../routes/archives/ArchivesPage';
import DataSetsPage from '../routes/datasets/DataSetsPage';
import TelescopesPage from '../routes/telescopes/TelescopesPage';
import ArchiveDetails from '../routes/details/ArchiveDetails';
import QueryPage from '../routes/query/QueryPage';

import { About } from './About';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    useParams
} from "react-router-dom";


// This site has multiple pages, all of which are rendered
// dynamically in the browser (not server rendered).
//
// Although the page does not ever refresh, notice how
// React Router keeps the URL up to date as you navigate
// through the site. This preserves the browser history,
// making sure things like the back button and bookmarks
// work properly.

function Main () {

    let basename = GetBaseName()
    if (basename === undefined) {
        return null
    }

    return (
        // basename defines which path is is added before the routing urls.
        <Router basename={basename}>
            <div>
                <NavigationBar/>

                {/*
                 A <Switch> looks through all its children <Route>
                 elements and renders the first one whose path
                 matches the current URL. Use a <Switch> any time
                 you have multiple routes, but you want only one
                 of them to render at a time
                 */}

                <Switch>
                    <Route exact path="/">
                        <ArchivesPage />
                    </Route>

                    <Route path="/telescopes">
                        <TelescopesPage />
                    </Route>

                    <Route path="/archives">
                        <ArchivesPage />
                    </Route>

                    <Route path="/datasets">
                        <DataSetsPage />
                    </Route>

                    <Route path="/query">
                        <QueryPage />
                    </Route>

                    <Route path="/about">
                        <About />
                    </Route>

                    <Route path="/archive/:uri" children={<DetailsForward />} />
                </Switch>
            </div>

            <footer><small>ASTRON - version 0.6.5 - 28 apr 2020</small></footer>
        </Router>
    );
}

// reroute to dataproduct details
function DetailsForward() {
    let { uri } = useParams();

    return (
        <ArchiveDetails uri={uri}/>
    );
}

export default Main;