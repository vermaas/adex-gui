import React, { useState } from 'react';
import { Button, Modal, FormGroup, FormControl, FormLabel } from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faWrench } from '@fortawesome/free-solid-svg-icons'

import { useGlobalReducer } from '../Store';
import { useLocalStorage } from '../hooks/useLocalStorage';

import { GetBackendHost } from '../utils/web'


export default function Configuration(props) {

    // use the global state
    const [ my_state , my_dispatch] = useGlobalReducer()
    const [esapApiHost, setEsapApiHost] = useLocalStorage('esap_backend_host','')

    const [show, setShow] = useState(false)
    const [host, setHost] = useState(GetBackendHost(""))

    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)

    const handleSaveClose = () => {
        // write the new host to localStorage
        let backend_host = host
        if (backend_host.endsWith("/")) {
            backend_host = backend_host.slice(0, -1);
        }

        setEsapApiHost(backend_host)
        setShow(false)
    }

    const changeHost = (host) => {
        setHost(host.target.value)
    }


    const getValidationState = () => {
        const length = host.length;
        if (length > 10) return 'success';
        else if (length > 5) return 'warning';
        else if (length > 0) return 'error';
    }


    let renderConfigButton =
        <Button variant="outline-info"
                onClick={handleShow}><FontAwesomeIcon icon={faWrench} />
            &nbsp;ESAP Gateway Host
        </Button>

    let renderConfigDialog =
        <Modal show={show} onHide={handleClose}>
            <Modal.Header>
                <Modal.Title>Configuration</Modal.Title>
            </Modal.Header>
            <Modal.Body>

                <form>
                    <FormGroup controlId="host" validationState={getValidationState()}>
                        <FormLabel>ESAP API host</FormLabel>
                        <FormControl
                            type="text"
                            value={host}
                            placeholder={"http://80.101.27.83/esap-api/"}
                            onChange={(host) => changeHost(host)}
                        />
                    </FormGroup>
                </form>

            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={handleSaveClose}>OK</Button>
                <Button variant="warning" onClick={handleClose}>Cancel</Button>
            </Modal.Footer>
        </Modal>


    return (
        <div>

            {renderConfigButton}
            {renderConfigDialog}
        </div>
    );
}


