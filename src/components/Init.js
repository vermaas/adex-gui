import React, {useState }  from 'react';
import '../App.css';

import { getBackendUrl } from '../utils/web'
import { useFetchArchives } from '../hooks/useFetchArchives';
import { useFetchConfiguration } from '../hooks/useFetchConfiguration';


export default function Init() {

    useFetchConfiguration(getBackendUrl("configuration"), {onMount: true})

    // note that the resource 'archives-uri' is fetched instead of the more (human friendly) 'archives'
    useFetchArchives(getBackendUrl("archives-uri"),{onMount:true})

    return null
}


