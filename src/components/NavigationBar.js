import React from 'react';

import { Navbar, Nav } from 'react-bootstrap';

import { NavLink } from "react-router-dom"
import { getBackendUrl } from '../utils/web'
import { GetLogo, GetTitle, GetNavigationBar, GetBaseName } from '../utils/config'


export function NavigationBar() {

    // construct a single navlink
    const addNav = (nav) => {
        return <Nav.Link as={NavLink} to={nav.route}>{nav.title}</Nav.Link>
    }

    // construct the navigation bar based on the configuration
    let navlist = GetNavigationBar()
    let navbar = navlist.map(nav => addNav(nav))

    let basename = "/"+GetBaseName()

    return (
        <Navbar bg="dark" variant="dark">
            <img alt='' src={GetLogo()}  height="40" className="d-inline-block align-top"/>
            <Navbar.Brand href={basename}>&nbsp;{GetTitle()}
            </Navbar.Brand>

            <Nav className="mr-auto">
                {navbar}
            </Nav>

        </Navbar>

    );
}