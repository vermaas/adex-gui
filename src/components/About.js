import React from 'react';
import { getBackendUrl,  GetBackendHost} from '../utils/web'
import Configuration from './Configuration';
import { GetBaseName } from '../utils/config'

export function About() {

    return (
        <div>

            <p></p>
            <p>
                <ul>
                    <li> GUI basename : {GetBaseName()} </li>
                    <li> ESAP Gateway: <a className="App-link" href={GetBackendHost()} target="_blank" rel="noopener noreferrer"> {GetBackendHost()}</a></li>
                </ul>
            </p>
            <p>
                Source and Documentation in ASTRON gitlab.
            </p>
            <p>
                <ul>
                    <li><a className="App-link" href="https://git.astron.nl/vermaas/adex-gui" target="_blank" rel="noopener noreferrer"> ADEX / ESAP-GUI</a></li>
                    <li><a className="App-link" href="https://git.astron.nl/vermaas/esap-gateway" target="_blank" rel="noopener noreferrer"> ESAP-gateway (API)</a></li>
                </ul>
            </p>
            <Configuration host = {getBackendUrl}  />
        </div>
    );
}