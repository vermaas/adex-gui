import React from 'react';
import {Card, Button } from 'react-bootstrap'

import { useGlobalReducer } from '../../Store';
import { SET_IMAGE_TYPE } from '../../reducers/GlobalStateReducer'

// display the main image
function MainImage(props) {
    let thumbnail =  props.archive.thumbnail
    return <a href={thumbnail} target=""_blank><img src={thumbnail} width="300"/></a>
}

// display a single archive on a card
export default function ImageCard(props) {

    const [ my_state , my_dispatch] = useGlobalReducer()


    return (

        <Card className="card-archive">
            <Card.Body>
                <tr>
                    <MainImage archive={props.archive} imageType={my_state.image_type}/>
                </tr>
            </Card.Body>

        </Card>

    );

}


