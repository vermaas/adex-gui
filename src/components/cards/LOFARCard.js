import React from 'react';
import {Card, Button } from 'react-bootstrap'

import DocumentationLink from '../buttons/DocumentationLink'

// display the main image
function CardImage(props) {
    return <img src="https://www.astron.nl/sites/default/files/styles/subpage/public/shared/Images/teaser_lofar.jpg?itok=2dckAEsk" width="300"/>
}


// display a single archive on a card
export default function WSRTCard(props) {

    return (

        <Card className="card-telescope">
            <Card.Body>
                <Card.Title>LOFAR</Card.Title>
                <CardImage/>
                <DocumentationLink url="https://www.astron.nl/telescopes/lofar"/>
                <hr></hr>
                <Card.Text>
                    LOFAR (Low Frequency Array) is currently the largest radio telescope operating at the lowest frequencies that can be observed from Earth. Unlike single-dish telescope, LOFAR is a multipurpose sensor network, with an innovative computer and network infrastructure that can handle extremely large data volumes.
                </Card.Text>

            </Card.Body>
        </Card>

    );

}


