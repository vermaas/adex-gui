import React from 'react';
import {Card, Button } from 'react-bootstrap'

// display the main image
function MainImage(props) {
    return <a href={props.url} target="_blank"><img src={props.url} width="300"/></a>
}

// display a single archive on a card
export default function DataProductImageCard(props) {

    return (

        <Card className="card-archive">
            <h5>{props.item.title}</h5>
            <Card.Body>
                <tr>
                    <MainImage url={props.item.thumbnail} />
                </tr>
            </Card.Body>

        </Card>

    );

}


