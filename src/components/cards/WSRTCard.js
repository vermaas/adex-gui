import React from 'react';
import {Card, Button } from 'react-bootstrap'

import DocumentationLink from '../buttons/DocumentationLink'

// display the main image
function CardImage(props) {
    return <img src="https://www.astron.nl/sites/default/files/styles/subpage/public/2019-04/ASTRON_Westerbork_Stills_30_1.jpg" width="300"/>
}


// display a single archive on a card
export default function WSRTCard(props) {

    return (

        <Card className="card-telescope">
            <Card.Body>
                 <Card.Title>WSRT</Card.Title>
                <CardImage/>
                <DocumentationLink url="https://www.astron.nl/telescopes/wsrt-apertif"/>
                <hr></hr>
                <Card.Text>
                    The Westerbork Synthesis Radio Telescope (WSRT) is a powerful radio telescope that uses a technique called "aperture synthesis" to generate radio images of the sky.
                </Card.Text>
            </Card.Body>
        </Card>

    );

}


