import React from 'react';
import {Card, Button } from 'react-bootstrap'

import DocumentationLink from '../buttons/DocumentationLink'

// display the main image
function CardImage(props) {
    return <img src="https://www.astron.nl/sites/default/files/styles/subpage/public/shared/Images/teaser_ska.jpg?itok=EO_jDSeC" width="300"/>
}


// display a single archive on a card
export default function WSRTCard(props) {

    return (

        <Card className="card-telescope">
            <Card.Body>

                <Card.Title>Square Kilometre Array</Card.Title>
                <CardImage/>
                <DocumentationLink url="https://www.astron.nl/telescopes/square-kilometre-array"/>
                <hr></hr>
                <Card.Text>
                    The Square Kilometre Array will be the most powerful radio telescope in the world. It will enable transformational science that will change our understanding of the Universe.
                </Card.Text>

            </Card.Body>
        </Card>

    );

}


