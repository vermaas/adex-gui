import React from 'react';
import {Card, Container, Row, Col } from 'react-bootstrap'

import DescriptionCard from './DescriptionCard'
import CatalogButton from '../buttons/CatalogButton'
import DetailsButton from '../buttons/ArchiveDetailsButton'


// display a single archive on a card
export default function ArchiveThumbnail(props) {

    return (

            <Card className="card-description">

                <Card.Body>
                    <h2>{props.archive.name}</h2>
                    <Container fluid>
                        <Row>
                            <Col sm={3} md={4} lg={6}>
                                <img src={props.archive.thumbnail} height={200} width={300} />
                                <h2> </h2>
                                <Row>
                                    <Col>
                                        <DetailsButton archive = {props.archive}/> &nbsp;
                                        <CatalogButton archive={props.archive}/>&nbsp;
                                    </Col>
                                </Row>
                            </Col>
                            <Col sm={3} md={4} lg={6}>
                                <DescriptionCard archive = {props.archive} />
                            </Col>
                        </Row>

                    </Container>
                </Card.Body>
            </Card>

    );
}

