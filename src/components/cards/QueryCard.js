import React from 'react';
import {Card, Table } from 'react-bootstrap'

import { useGlobalReducer } from '../../Store';

import DocumentationLink from '../buttons/DocumentationLink'


// display a single query on a card
export default function QueryCard(props) {

    const [ my_state , my_dispatch] = useGlobalReducer()

    // only show if there is content
    if (!props.query) {
        return null
    }

    return (

        <Card className="card-description">
            <Card.Body>
                <Card.Title>{props.query.dataset_name}</Card.Title>
                <Card.Text>
                    <Table striped bordered hover size="sm">
                        <tbody>
                        <tr>
                            <td className="key">ID</td>
                            <td className="value">{props.query.query_id}</td>
                        </tr>
                        <tr>
                            <td className="key">Query</td>
                            <td className="value">{props.query.query}</td>
                        </tr>
                        <tr>
                            <td className="key">Description</td>
                            <td className="value">{props.query.res_description}</td>
                        </tr>
                        </tbody>
                    </Table>

                </Card.Text>
                <DocumentationLink url={props.query.reference_url}/>&nbsp;
            </Card.Body>

        </Card>

    );

}


