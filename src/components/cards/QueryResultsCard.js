import React from 'react';
import {Card, Button } from 'react-bootstrap'

import { useGlobalReducer } from '../../Store';

import { getBackendUrl } from '../../utils/web'
import LoadingSpinner from '../../components/LoadingSpinner';
import DataSetsGrid from '../../routes/datasets/DataSetsGrid'
import { useFetchRunQuery } from '../../hooks/useFetchRunQuery';
import QueryResultsList from '../../routes/query/QueryResultsList'

// display the results of a query on a card
export default function QueryResultsCard(props) {

    const [ my_state , my_dispatch] = useGlobalReducer()

    let renderQueryResults
    if (my_state.fetched_query_results[props.data.dataset]) {

        renderQueryResults= <div>
            <a href={my_state.fetched_query_results[props.data.dataset][0].results}>{my_state.fetched_query_results[props.data.dataset][0].results}</a>
        </div>
    }

    let renderSpinner
    if (my_state.status_run_query === "fetching") {
        renderSpinner = <LoadingSpinner/>
    }


    return (

        <Card className="card-description">
            <Card.Body>
                <div >
                    {renderSpinner}
                    {renderQueryResults}
                </div>
            </Card.Body>
        </Card>

    );

}


