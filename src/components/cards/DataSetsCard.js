import React from 'react';
import {Card, Button } from 'react-bootstrap'

import { useGlobalReducer } from '../../Store';

import { getBackendUrl } from '../../utils/web'
import LoadingSpinner from '../../components/LoadingSpinner';
import DataSetsGrid from '../../routes/datasets/DataSetsGrid'
import { useFetchDataSets } from '../../hooks/useFetchDataSets';


// display a list of datasets on a card
export default function DataSetsCard(props) {

    const [ my_state , my_dispatch] = useGlobalReducer()
    let query = "dataset_archive__uri="+props.archive.uri
    useFetchDataSets(getBackendUrl("datasets-uri?"+query),{onMount:true})

    let renderDataSetsGrid
    if (my_state.status_datasets==='fetched_datasets') {
        renderDataSetsGrid= <div>
            <DataSetsGrid data={my_state.fetched_datasets} expand={true} all_columns={false} />
        </div>
    }

    let renderSpinner
    if (my_state.status_datasets === "fetching_datasets") {
        renderSpinner = <LoadingSpinner/>
    }


    return (

        <Card className="card-description">
            <Card.Body>
                <div >
                    {renderSpinner}
                    {renderDataSetsGrid}
                </div>
            </Card.Body>
        </Card>

    );

}


