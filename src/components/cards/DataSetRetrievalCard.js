import React from 'react';
import {Card, Button } from 'react-bootstrap'

import { useGlobalReducer } from '../../Store';

import CatalogButton from '../buttons/CatalogButton'

// display a single archive on a card
export default function DataSetRetrievalCard(props) {

    const [ my_state , my_dispatch] = useGlobalReducer()

    // only show if there is content
    if (!props.archive.retrieval_description) {
        return null
    }

    return (

        <Card className="card-description">
            <Card.Body>
                <Card.Text>
                    {props.archive.retrieval_description}
                </Card.Text>
                <CatalogButton archive={props.archive}/>&nbsp;
            </Card.Body>

        </Card>

    );

}


