import React from 'react';
import {Card, Button } from 'react-bootstrap'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faImage, faArrowsAlt, faProjectDiagram } from '@fortawesome/free-solid-svg-icons'

import { useGlobalReducer } from '../../Store';

import DocumentationLink from '../buttons/DocumentationLink'
import ArchiveApiButton from '../buttons/ArchiveApiButton'
import DataSetApiButton from '../buttons/DataSetApiButton'

// display a single archive on a card
export default function DescriptionCard(props) {

    const [ my_state , my_dispatch] = useGlobalReducer()

    let short_description
    let long_description
    let renderApiButton
    let renderDocumentationLink

    if (props.archive) {
        // only show if there is content
        if (!props.archive.long_description) {
            return null
        }

        short_description = props.archive.short_description
        long_description = props.archive.long_description
        renderApiButton = <ArchiveApiButton archive={props.archive}/>
        renderDocumentationLink =  <DocumentationLink archive={props.archive}/>
    }

    if (props.dataset) {
        // only show if there is content
        if (!props.dataset.long_description) {
            return null
        }

        short_description = props.dataset.short_description
        long_description = props.dataset.long_description
        renderApiButton = <DataSetApiButton dataset={props.dataset}/>
        renderDocumentationLink =  <DocumentationLink archive={props.dataset}/>
    }

    return (

        <Card className="card-description">
            <Card.Body>
                <Card.Title>{short_description}</Card.Title>
                <Card.Text>
                    {long_description}
                </Card.Text>
                {renderDocumentationLink}&nbsp;
                {renderApiButton}
            </Card.Body>
        </Card>

    );

}


