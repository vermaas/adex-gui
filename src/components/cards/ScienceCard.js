import React from 'react';
import {Card, Button } from 'react-bootstrap'

import { useGlobalReducer } from '../../Store';

import DocumentationLink from '../buttons/DocumentationLink'


// display a single archive on a card
export default function ScienceCard(props) {

    const [ my_state , my_dispatch] = useGlobalReducer()

    // only show if there is content
    if (!props.archive.scientific_description) {
        return null
    }

    return (

        <Card className="card-description">
            <Card.Body>
                <Card.Title>Science</Card.Title>
                <Card.Text>
                    {props.archive.scientific_description}
                </Card.Text>
                <DocumentationLink archive={props.archive}/>&nbsp;
            </Card.Body>

        </Card>

    );

}


