import React from 'react';
import {Button } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'


// show a clickable icon
export default function AddToBasketButton(props) {

    // only show the  button when there is content
    if (!props) {
        return null
    }

    return (
        <Button variant="outline-primary" > <FontAwesomeIcon icon={faShoppingCart} /> Add To Basket</Button>
    );
}

