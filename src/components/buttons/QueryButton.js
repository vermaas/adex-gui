import React from 'react';
import {Button } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons'

import { getBackendUrl } from '../../utils/web'
import { useGlobalReducer } from '../../Store';

import { useFetchRunQuery } from '../../hooks/useFetchRunQuery';

// show a clickable icon
export default function QueryButton(props) {
    // custom hooks
    const [my_state, my_dispatch] = useGlobalReducer()
    const fetchRunQuery = useFetchRunQuery(getBackendUrl("run-query"), {})


    const runQuery = (input_query) => {

        let query = input_query.query

        // if this is a VO query, then cut off everything but the ADQL itself so that the backend can parse it.
        if (input_query.protocol.toUpperCase()==='ADQL') {
            //let query = input_query.query
            let s = query.split('&QUERY=')
            query = s[1]
        }

        let url = getBackendUrl("run-query") + "?dataset_uri=" + input_query.dataset + "&query=" + query

        // encode it
        let s = encodeURI(url)

        alert(s)

        fetchRunQuery.fetchData(s, input_query.dataset, my_state.fetched_query_results)

    }

    // only show the  button when there is content
    if (!props) {
        return null
    }

    return (
        <Button variant="warning" onClick={() => runQuery(props.input_query)}> <FontAwesomeIcon icon={faSyncAlt} /> Run Query</Button>
    );
}

