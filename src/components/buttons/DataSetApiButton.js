import React from 'react';
import { Link } from "react-router-dom"
import { Button } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSitemap } from '@fortawesome/free-solid-svg-icons'

import { getBackendUrl } from '../../utils/web'

// show a clickable icon
export default function DataSetApiButton(props) {

    // generate the api link
    const getAPI = (dataset) => {
        let url = getBackendUrl("datasets/")
        let api_link = url + dataset.id
        return api_link
    }

    return (

        <a href={getAPI(props.dataset)} target="_blank" rel="noopener noreferrer">
            <Button variant="outline-info"><FontAwesomeIcon icon={faSitemap} /> API</Button>
        </a>
    );
}

