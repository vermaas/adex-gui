import React from 'react';
import { Link } from "react-router-dom"
import { Button } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAddressCard, faFileAlt, faListAlt } from '@fortawesome/free-solid-svg-icons'

import { useGlobalReducer } from '../../Store';
import { SET_ACTIVE_ARCHIVE } from '../../reducers/GlobalStateReducer'

// show a clickable icon
export default function ArchiveDetailsButton(props) {
    const [ my_state , my_dispatch] = useGlobalReducer()

    const handleClick = (archive) => {
        // dispatch current archive to the global store
        my_dispatch({type: SET_ACTIVE_ARCHIVE, archive: archive})
    }

    // generate the details link to forward to
    const getLink = (archive) => {
        let details_link = "/archive/"+archive.uri
        return details_link
    }

    // only show the  button when there is content
    if (!props.archive.uri) {

        return null
    }



    return (

        <Link to={() => getLink(props.archive)}>
            <Button variant="outline-info" onClick={() => handleClick(props.archive)}><FontAwesomeIcon icon={faListAlt} /> Details</Button>
        </Link>
    );
}

