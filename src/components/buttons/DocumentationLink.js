import React from 'react';
import {Button } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBook, faInfoCircle } from '@fortawesome/free-solid-svg-icons'

// show a clickable icon
export default function DocumentationLink(props) {
    let url

    // first check if a 'archive=' was given as parameter and use its documentation_url as the url
    if ((props.archive) && (props.archive.documentation_url)) {
        url = props.archive.documentation_url
    }

    // second, check if a 'url=' is given, which will override the previous
    if (props.url) {
        url = props.url
    }

    // if no url was given at all, abort
    if (!url) {
        return null
    }

    return (
        <a href = {url} title="Documentation" target="_blank" rel="noopener noreferrer">
            <Button variant="outline-info"> <FontAwesomeIcon icon={faInfoCircle} /> Documentation</Button>
        </a>
    );
}

