import React from 'react';
import {Button } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons'

import { GetBackendHost } from '../../utils/web'

import { useGlobalReducer } from '../../Store';
import { useFetchRunQuery } from '../../hooks/useFetchRunQuery';

import { SET_QUERIES_TO_RUN, SET_QUERIES_HAVE_RESULTS } from '../../reducers/GlobalStateReducer';

// show a clickable icon
export default function RunQueriesButton(props) {

    // custom hooks
    const [my_state, my_dispatch] = useGlobalReducer()
    const backend_host = GetBackendHost()
    const fetchRunQuery = useFetchRunQuery(getBackendUrl("run-query"), {})

    function getBackendUrl(resource) {
        return backend_host.concat(resource)
    }

    const runQuery = (input_query) => {

        let query = input_query.query
        let access_url = query.split('?')[0]

        // if this is a VO query, then cut off everything but the ADQL itself so that the backend can parse it.
        if (input_query.esap_service.toUpperCase()==='VO') {
            //let query = input_query.query
            let s = query.split('&QUERY=')
            query = s[1]
        } else

        if (input_query.esap_service.toUpperCase()==='VO_REG') {
            //let query = input_query.query
            let s = query.split('&QUERY=')
            query = s[1]
        }

        let url = getBackendUrl("run-query")
            + "?dataset_uri=" + input_query.dataset
            + "&dataset_name=" + input_query.dataset_name
            + "&access_url=" + access_url
            + "&query=" + query


        // encode it
        // fetchRunQuery.fetchData(encodeURI(url), input_query.dataset)
        fetchRunQuery.fetchData(encodeURI(url), input_query.dataset_name)

    }


    // run all the queries in the selected list of queries
    const runQueries = (queries) => {
        let queries_to_run = []
        for (var i=0; i< queries.length; i++) {
            //alert(queries[i].dataset_name)
            queries_to_run.push(queries[i])
        }

        // store the list of datasets to query. This can later be used as keys to iterate over the results
        my_dispatch({type: SET_QUERIES_TO_RUN, queries_to_run: queries_to_run})
        my_dispatch({type: SET_QUERIES_HAVE_RESULTS, queries_have_results: true})

        // run the queries by firing a asyc request per query
        queries.map(query => runQuery(query))
    }

    // only show the  button when there is content
    if (!props) {
        return null
    }

    return (
        <Button variant="outline-primary" onClick={() => runQueries(props.queries)}> <FontAwesomeIcon icon={faSyncAlt} /> &nbsp;Run Queries</Button>
    );
}

