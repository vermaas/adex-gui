import React from 'react';
import { Link } from "react-router-dom"
import { Button } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAddressCard, faFileAlt, faListAlt } from '@fortawesome/free-solid-svg-icons'

// show a clickable icon
export default function DataSetDetailsButton(props) {

    // generate the details link to forward to
    const getLink = (dataset) => {
        let details_link = "/dataset/"+dataset.uri
        return details_link
    }

    // only show the  button when there is content
    if (!props.dataset.uri) {
        return null
    }



    return (

        <Link to={() => getLink(props.dataset)}>
            <Button variant="outline-info"><FontAwesomeIcon icon={faListAlt} /> Details</Button>
        </Link>
    );
}

