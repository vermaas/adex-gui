import React from 'react';
import {Button } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArchive } from '@fortawesome/free-solid-svg-icons'

// show a clickable icon
export default function CatalogButton(props) {

    // only show the  button when there is content
    if (!props.archive.catalog_url) {
        return null
    }

    return (
        <a href = {props.archive.catalog_url} title={props.archive.catalog_name} target="_blank" rel="noopener noreferrer">
            <Button variant="outline-info"> <FontAwesomeIcon icon={faArchive} /> Visit Catalog</Button>
        </a>
    );
}

