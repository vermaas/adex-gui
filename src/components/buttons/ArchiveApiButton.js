import React from 'react';
import { Link } from "react-router-dom"
import { Button } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSitemap } from '@fortawesome/free-solid-svg-icons'

import { getBackendUrl } from '../../utils/web'

// show a clickable icon
export default function ArchiveApiButton(props) {

    // generate the api link
    const getAPI = (archive) => {
        let url = getBackendUrl("archives/")
        let api_link = url + archive.id
        return api_link
    }

    return (

        <a href={getAPI(props.archive)} target="_blank" rel="noopener noreferrer">
            <Button variant="outline-info"><FontAwesomeIcon icon={faSitemap} /> API</Button>
        </a>
    );
}

