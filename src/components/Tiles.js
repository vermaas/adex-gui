import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

import DataProductImageCard from './cards/DataProductImageCard'

// loop through a list of observations and create a Card with a clickable thumbnail for all of them
export default function Tiles(props) {

    return (
        <div>
            <Container fluid>
                <Row>
                    {
                        props.data.map((item) => {

                            let renderImage
                            if (item.thumbnail) {
                                renderImage = <Col lg={true}><DataProductImageCard item = {item}/></Col>
                            }
                            return (
                                <div>
                                    {renderImage}
                                </div>
                            );
                        })
                    }
                </Row>
            </Container>
        </div>
    );

}

