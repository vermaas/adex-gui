// Nico Vermaas - 28 oct 2019
// This is the reducer for the global state providor.

// possible actions
export const SET_STATUS_CONFIGURATION = 'SET_STATUS_CONFIGURATION'
export const SET_CONFIGURATION = 'SET_CONFIGURATION'

export const SET_STATUS_ARCHIVES = 'SET_STATUS'
export const SET_ACTIVE_ARCHIVE = 'SET_ACTIVE_ARCHIVE'
export const SET_FETCHED_ARCHIVES = 'SET_FETCHED_ARCHIVES'

export const SET_STATUS_DATASETS = 'SET_STATUS_DATASETS'
export const SET_FETCHED_DATASETS = 'SET_FETCHED_DATASETS'

export const SET_ESAP_QUERY = 'SET_ESAP_QUERY'
export const SET_QUERIES_HAVE_RESULTS = 'SET_QUERIES_HAVE_RESULTS'
export const SET_STATUS_QUERY = 'SET_STATUS_QUERY'
export const SET_STATUS_RUN_QUERY = 'SET_STATUS_RUN_QUERY'
export const SET_QUERIES_TO_RUN = 'SET_QUERIES_TO_RUN'
export const SET_FETCHED_QUERY_INPUT = 'SET_FETCHED_QUERY_INPUT'
export const SET_FETCHED_QUERY_RESULTS = 'SET_FETCHED_QUERY_RESULTS'

export const initialState = {
        config: undefined,
        status_configuration: "unfetched",
        status: "unfetched",
        status_datasets: "unfetched",
        status_query: "unfetched",
        status_run_query: undefined,
        id: undefined,
        archive: undefined,
        fetched_archives: undefined,
        filtered_archives: undefined,
        fetched_datasets: undefined,
        filtered_datasets: undefined,
        queries_to_run: undefined,
        fetched_query: undefined,
        fetched_query_results: undefined,
        queries_have_results: false,
        esap_query: "",
}

export const reducer = (state, action) => {
    console.log(action)
    switch (action.type) {

        case SET_STATUS_CONFIGURATION:
            return {
                ...state,
                status: action.status
            };

        case SET_CONFIGURATION:
            return {
                ...state,
                config: action.config
            };

        case SET_STATUS_ARCHIVES:
            return {
                ...state,
                status: action.status
            };

        case SET_STATUS_DATASETS:
            return {
                ...state,
                status_datasets: action.status_datasets
            };

        case SET_STATUS_QUERY:
            return {
                ...state,
                status_query: action.status_query
            };

        case SET_ACTIVE_ARCHIVE:
            return {
                ...state,
                archive: action.archive
            };

        case SET_FETCHED_ARCHIVES:
            return {
                ...state,
                fetched_archives: action.fetched_archives
            };

        case SET_FETCHED_DATASETS:
            return {
                ...state,
                fetched_datasets: action.fetched_datasets
            };

        case SET_FETCHED_QUERY_INPUT:
            return {
                ...state,
                fetched_query: action.fetched_query
            };


        case SET_QUERIES_TO_RUN:
            return {
                ...state,
                queries_to_run: action.queries_to_run
            };

        case SET_QUERIES_HAVE_RESULTS:
            return {
                ...state,
                queries_have_results: action.queries_have_results
            };

        case SET_STATUS_RUN_QUERY:
            // construct the status_run_query object in such a way that it stores results per query.
            var state_to_return =  {...state}
            try {
                // example: "UCL Astro TAP:fetched"
                let status = action.status_run_query.split(':')
                let key = "status_run_query." + status[0]

                state_to_return[key]= status[1]

            } finally {
                return state_to_return
            }

        case SET_FETCHED_QUERY_RESULTS:
            // construct the fetched_query_results object in such a way that it stores results per query.
            var state_to_return =  {...state}
            try {

                // the dataset_name is used in the key for the state, so that the state holds results per dataset_name
                // because 'dataset' isn't unique in case of the 'vo_reg' dataset which yields many dataset names.

                let dataset_name = action.fetched_query_results[0].dataset_name
                let key = "fetched_query_results." + dataset_name

                state_to_return[key]= action.fetched_query_results

            } finally {
                return state_to_return
            }


        case SET_ESAP_QUERY:
            return {
                ...state,
                esap_query: action.esap_query
            };

        default:
            return state;
    }
};