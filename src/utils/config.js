import React from 'react';
import { useGlobalReducer } from '../Store';

import default_logo from '../assets/esap_logo.png';
import default_query_schema from '../routes/query/schema_datasets_obsolete.json';
// note that the function name must start with a capital,
// otherwise the function is not recognized as a react hook function.

// the subdirectory where the frontend is deployed
// every url will be relative to this basename, for instance "http://esap.astron.nl/esap-gui/query
export function GetBaseName() {
    const [my_state, my_dispatch] = useGlobalReducer()
   
    let basename = undefined

    try {
        basename = my_state.config.configuration.frontend_basename
    } catch (e) {
        basename = undefined
    }

    return basename
}


export function GetLogo() {
    const [my_state, my_dispatch] = useGlobalReducer()

    let logo = default_logo

    try {
        logo = my_state.config.configuration.logo
        if (logo===undefined) {
            logo = default_logo
        }

    } catch (e) {
        logo = default_logo
    }

    return logo
}


export function GetTitle() {
    const [my_state, my_dispatch] = useGlobalReducer()

    let default_title = ''
    let title = default_title

    try {
        title = my_state.config.configuration.title
        if (title===undefined) {
            title = default_title
        }

    } catch (e) {
        title = default_title
    }

    return title
}


export function GetNavigationBar() {
    const [my_state, my_dispatch] = useGlobalReducer()

    let default_navbar = [
        {'title': 'Archives', 'route': '/archives'},
        {'title': 'Datasets', 'route': '/datasets'},
        {'title': 'Query', 'route': '/query'},
        {'title': 'About', 'route': '/about'}
    ]

    let navbar = default_navbar

    try {
        navbar = my_state.config.configuration.navbar
        if (navbar===undefined) {
            navbar = default_navbar
        }

    } catch (e) {
        navbar = default_navbar
    }

    return navbar
}


export function GetQuerySchema() {
    const [my_state, my_dispatch] = useGlobalReducer()


    let query_schema = default_query_schema

    try {
        query_schema = my_state.config.configuration.query_schema

        if (query_schema===undefined) {
            query_schema = default_query_schema
        }

    } catch (e) {
        query_schema = default_query_schema
    }

    return query_schema
}