
import { useLocalStorage } from '../hooks/useLocalStorage';

export function GetBackendHost() {

    const [esapApiHost, setEsapApiHost] = useLocalStorage('esap_backend_host','')

    // the default backend hostname for production is the host where this frontend runs.
    // When this has been changed with the configuration button, then the backend hostname
    // will be stored in 'localStorage'. So check their first, and if not found use the default.

    if (esapApiHost) {
        return esapApiHost + '/'
    }

    if (process.env.NODE_ENV === 'development') {
        return "http://localhost:8000/esap-api/"
    }

    let protocol = window.location.protocol;
    let slashes = protocol.concat("//");
    let host = slashes.concat(window.location.hostname)+"/esap-api/";
    return host

}

export function getBackendUrl(resource) {
    return GetBackendHost().concat(resource)
}