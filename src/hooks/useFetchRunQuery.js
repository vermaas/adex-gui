import React, {useState, useEffect }  from 'react';
import { useGlobalReducer } from '../Store';
import { SET_FETCHED_QUERY_RESULTS, SET_STATUS_RUN_QUERY } from '../reducers/GlobalStateReducer';

/*

Example of url to fetch:

  http://localhost:8000/esap-api/run-query?dataset=ivao.obscore&query=https://vo.astron.nl/__system__/tap/run/tap/sync?lang=ADQL&REQUEST=doQuery&QUERY=SELECT TOP 10 * from ivoa.obscore where obs_title='TGSSADR_R01D36_5x5'

Example of json response:

 {
 "query_results": [
     {
         "dataset": "ivoa.obscore",
     }
    ]
 }
*/

export const useFetchRunQuery = (url, options) => {
    // use global state
    const [ my_state , my_dispatch] = useGlobalReducer()
    const [response, setResponse] = React.useState(null);
    const [error, setError] = React.useState(null);

    const fetchData = async (url, query) => {
        let query_status_object={}
        let query_status = query+":fetching..."
        query_status_object[query]=query_status

        try {

            // dispatch the status to the global state
            my_dispatch({type: SET_STATUS_RUN_QUERY, status_run_query: {}})

            const res = await fetch(url, options);
            const json = await res.json();

            // this sets the json in the response, which is in itself a useState hook
            setResponse(json);

            // dispatch the fetched data (per query) and the status to the global state
            my_dispatch({type: SET_FETCHED_QUERY_RESULTS, fetched_query_results: json.query_results})
            my_dispatch({type: SET_STATUS_RUN_QUERY, status_run_query: query+":fetched"})


        } catch (error) {
            setError(error);
            query_status = query+":error"
            my_dispatch({type: SET_STATUS_RUN_QUERY, status_run_query: query_status})
        }
    };

    React.useEffect(() => {
        if(options.onMount){
            fetchData(url);
        }
    }, []);

    return { response, error, fetchData };
};