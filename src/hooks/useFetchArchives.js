import React, {useState, useEffect }  from 'react';
import { useGlobalReducer } from '../Store';
import { SET_FETCHED_ARCHIVES, SET_STATUS_ARCHIVES } from '../reducers/GlobalStateReducer';

export const useFetchArchives = (url, options) => {
    // use global state
    const [ my_state , my_dispatch] = useGlobalReducer()
    const [response, setResponse] = React.useState(null);
    const [error, setError] = React.useState(null);

    const fetchData = async (url) => {
        try {

            // dispatch the status to the global state
            my_dispatch({type: SET_STATUS_ARCHIVES, status: 'fetching'})
            const res = await fetch(url, options);
            const json = await res.json();

            // this sets the json in the response, which is in itself a useState hook
            setResponse(json);
            console.log('useFetchArchives - fetched_archives')
            // dispatch the fetched data and the status to the global state
            my_dispatch({type: SET_FETCHED_ARCHIVES, fetched_archives: json.results})
            my_dispatch({type: SET_STATUS_ARCHIVES, status: 'fetched_archives'})
        } catch (error) {
            setError(error);
            my_dispatch({type: SET_STATUS_ARCHIVES, status: 'error fetching archives'})
        }
    };

    React.useEffect(() => {
        if(options.onMount){
            fetchData(url);
        }
    }, []);

    return { response, error, fetchData };
};