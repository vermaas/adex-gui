import React, {useState, useEffect }  from 'react';
import { useGlobalReducer } from '../Store';
import { SET_FETCHED_QUERY_INPUT, SET_STATUS_QUERY } from '../reducers/GlobalStateReducer';

/*

Example of url to fetch:

  http://localhost:8000/esap-api/create-query/?target=M51&archive_uri=astron_vo

Example of json response:

 {
 "query_input": [
     {
         "dataset": "ivoa.obscore",
         "service_url": "https://vo.astron.nl/__system__/tap/run/tap",
         "protocol": "adql",
         "query": "https://vo.astron.nl/__system__/tap/run/tap/sync?lang=ADQL&REQUEST=doQuery&QUERY=SELECT TOP 10 * from ivoa.obscore where target_name='M51'"
     }
    ]
 }
*/

export const useFetchCreateQuery = (url, options) => {
    // use global state
    const [ my_state , my_dispatch] = useGlobalReducer()
    const [response, setResponse] = React.useState(null);
    const [error, setError] = React.useState(null);

    const fetchData = async (url) => {
        try {
            // dispatch the status to the global state
            my_dispatch({type: SET_STATUS_QUERY, status_query: 'fetching'})
            const res = await fetch(url, options);
            const json = await res.json();

            // this sets the json in the response, which is in itself a useState hook
            setResponse(json);

            // dispatch the fetched data and the status to the global state
            my_dispatch({type: SET_FETCHED_QUERY_INPUT, fetched_query: json.query_input})
            my_dispatch({type: SET_STATUS_QUERY, status_query: 'fetched_query'})
        } catch (error) {
            setError(error);
            my_dispatch({type: SET_STATUS_QUERY, status_query: 'error fetching query'})
        }
    };

    React.useEffect(() => {
        if(options.onMount){
            fetchData(url);
        }
    }, []);

    return { response, error, fetchData };
};