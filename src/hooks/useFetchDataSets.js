import React, {useState, useEffect }  from 'react';
import { useGlobalReducer } from '../Store';
import { SET_FETCHED_DATASETS, SET_STATUS_DATASETS } from '../reducers/GlobalStateReducer';

export const useFetchDataSets = (url, options) => {
    // use global state
    const [ my_state , my_dispatch] = useGlobalReducer()
    const [response, setResponse] = React.useState(null);
    const [error, setError] = React.useState(null);

    const fetchData = async (url) => {
        try {

            // dispatch the status to the global state
            my_dispatch({type: SET_STATUS_DATASETS, status_datasets: 'fetching_datasets'})
            const res = await fetch(url, options);
            const json = await res.json();

            // this sets the json in the response, which is in itself a useState hook
            setResponse(json);

            // dispatch the fetched data and the status to the global state
            console.log('useFetchDataSets - fetched_datasets')
            my_dispatch({type: SET_FETCHED_DATASETS, fetched_datasets: json.results})
            my_dispatch({type: SET_STATUS_DATASETS, status_datasets: 'fetched_datasets'})
        } catch (error) {
            setError(error);
            my_dispatch({type: SET_STATUS_DATASETS, status_datasets: 'error fetching datasets'})
        }
    };

    React.useEffect(() => {
        if(options.onMount){
            fetchData(url);
        }
    }, []);

    return { response, error, fetchData };
};