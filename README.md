# ADEX-GUI

Astron Data Explorer - GUI

Prototype for a landing page to discover the ASTRON datasources (WSRT, Apertif, LOFAR, VO, ...).

This frontend (ReactJS) connects to the ESAP-gateway (Django).

## Documentation
* frontend: https://git.astron.nl/vermaas/adex-gui/-/wikis/ESAP-GUI

See also:
* backend: https://git.astron.nl/vermaas/esap-gateway/wikis/ESAP-gateway
